# OpenML dataset: water_2

https://www.openml.org/d/45166

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Water Bayesian Network. Sample 2.**

bnlearn Bayesian Network Repository reference: [URL](https://www.bnlearn.com/bnrepository/discrete-medium.html#water)

- Number of nodes: 32

- Number of arcs: 66

- Number of parameters: 10083

- Average Markov blanket size: 7.69

- Average degree: 4.12

- Maximum in-degree: 5

**Authors**: F. V. Jensen, U. Kjaerulff, K. G. Olesen and J. Pedersen.

**Please cite**: ([URL](https://www.semanticscholar.org/paper/MIDAS%3A-An-Influence-Diagram-for-Management-of-in-Jensen-Jensen/a08ce2d88c66bb5bf7968bdadee87c2f91caeb2c)): F. V. Jensen, U. Kjaerulff, K. G. Olesen and J. Pedersen. Et Forprojekt Til et Ekspertsystem for Drift af Spildevandsrensning (An Expert System for Control of Waste Water Treatment - A Pilot Project). Technical Report, Judex Datasystemer A/S, Aalborg, 1989. In Danish.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/45166) of an [OpenML dataset](https://www.openml.org/d/45166). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/45166/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/45166/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/45166/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

